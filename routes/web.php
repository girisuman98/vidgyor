<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/video-lists-fetch', 'HomeController@videosList')->name('video-lists-fetch');
Route::post('/video-create', 'HomeController@videoStore')->name('video-create');
Route::post('/video-edit-details', 'HomeController@videoEdit')->name('video-edit-details');
Route::post('/video-update', 'HomeController@videoUpdate')->name('video-update');
