<?php

namespace App\Http\Controllers;

use DB;
use App\Video;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
	
	public function videosList(Request $request)
	{
		$videos = DB::table('videos');
		if($request->search_word)
		{
			$videos = $videos->where('title', 'like', '%'.$request->search_word.'%');
		}
		if($request->created_on)
		{
			$videos = $videos->whereDate('created_at', $request->created_on);
		}
		if($request->duration)
		{
			$videos = $videos->where('duration', $request->duration);
		}
		$videos = $videos->paginate(5);
		return view('videos',compact('videos'));
	}
	
	public function videoStore(Request $request)
	{
		if($request->hasFile('file_video') && $request->duration_video){
            
            $getvideoName = time().'.'.$request->file_video->getClientOriginalExtension();
            $request->file_video->move(public_path('/videos'), $getvideoName);
			
			$video = new Video();
			$video->file = $getvideoName;
			$video->duration = $request->duration_video;
           	$video->save();
			
			return 1;
        }
		return 0;
	}
	
	public function videoEdit(Request $request)
	{
		$data = Video::find($request->id);
		return view('videoEdit',compact('data'));
	}
	
	public function videoUpdate(Request $request)
	{
		if($request->video_id)
		{
            $video = Video::find($request->video_id);
			if($video)
			{
				if($request->hasFile('file_image'))
				{
					$getimageName = time().'.'.$request->file_image->getClientOriginalExtension();
					$request->file_image->move(public_path('/images/thumbnails'), $getimageName);
					$video->thumbnail = $getimageName;
				}
				$video->tags = $request->tags;
				$video->categories = $request->categories;
				$video->title = $request->title;
				$video->description = $request->description;
				$video->save();
				return 1;
			}
			return 0;
        }
		return 0;
	}
	
}
