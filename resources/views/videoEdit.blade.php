<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <video width="150" controls>
				  <source src="{{ asset('videos') }}/{{ $data->file }}" type="video/mp4">
				</video>
            </div>
			<div class="card mt-3">
				<h6 class="font-weight-bold pt-2 pl-2" style="color:#271284;">About this Video</h6>
                <div class="card-body p-2" style="background: #EAF0F6;">
					<p style="font-size: 12px;margin-bottom: 5px;">ID: {{ $data->id }}</p>
					<p style="font-size: 12px;margin-bottom: 5px;">Created On: {{ $data->created_at }}</p>
					<p style="font-size: 12px;margin-bottom: 5px;">Last Updated On: {{ $data->updated_at }}</p>
					<p style="font-size: 12px;margin-bottom: 5px;">Duration: {{ $data->duration }}</p> 
                </div>
            </div>
        </div>
		<div class="col-md-9">
            <div class="card p-3">
                <form action="{{ route('video-update') }}" method="post" enctype="multipart/form-data" id="update_video">
				@csrf
					<input type="hidden" name="video_id" id="video_id" value="{{ $data->id }}" />
					<div class="input-group form-sm form-2">
					  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Title" name="title" value="{{ $data->title }}" id="title" aria-label="Title" required>
					</div>
					<div class="input-group form-sm form-2 mt-3">
					  <textarea class="form-control my-0 py-1 red-border" placeholder="Description" name="description" id="description" aria-label="Description" required>{{ $data->description }}</textarea>
					</div>
					<div class="input-group form-sm form-2 mt-3">
					  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Tags" name="tags" id="tags" aria-label="Tags" required value="{{ $data->tags }}">
					</div>
					<div class="input-group form-sm form-2 mt-3">
					  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Categories" name="categories" id="categories" aria-label="Categories" required value="{{ $data->categories }}">
					</div>
					<div class="input-group  mt-3">
					  <div class="input-group-prepend">
						<span class="input-group-text" id="inputGroupFileAddon01">Upload Thumbnail</span>
					  </div>
					  <div class="custom-file">
						<input type="file" class="custom-file-input" name="file_image" id="file_image"
						  aria-describedby="inputGroupFileAddon01" accept="image/*" onchange="ValidateSizeImage(this)">
						<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
					  </div>
					</div>
					<button type="submit" class="btn pull-right mt-3 mr-0" style="background: rgb(39, 18, 132);font-size: 11px;padding: 10px 37px;">Update</button>
				</form>
            </div>
        </div>
    </div>
</div>
<script>
$("#update_video").submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
	
	$.ajax({
		url : post_url,
		type: request_method,
		data : new FormData($("#update_video")[0]),
		contentType: false,
		cache: false,
		processData: false,
		async: false,
		success : function(data){
			if(data == '1')
			{
				swal("Success!"," Updated Successfully", "success");
				$("#editVideoModal").modal('toggle');
				videosList();
			}
			else
			{
				swal("Error!", "Something went wrong", "error");
			}
		},
	});
});
</script>