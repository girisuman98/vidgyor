@if(count($videos) > 0)
<table id="dtBasicExample" class="table" cellspacing="0" width="100%">
  <thead style="background: rgb(234, 240, 246);">
	<tr>
	  <th class="th-sm">Thumbnail
	  </th>
	  <th class="th-sm">Title
	  </th>
	  <th class="th-sm">ID
	  </th>
	  <th class="th-sm">Duration
	  </th>
	  <th class="th-sm">Created On
	  </th>
	  <th class="th-sm">Action
	  </th>
	</tr>
  </thead>
  <tbody>
  @foreach($videos as $video)
	<tr>
	  <td>@if($video->thumbnail)<img src="{{ asset('images/thumbnails') }}/{{ $video->thumbnail }}" alt="{{ $video->title }}" style="width: 50px;height: 40px;">@endif</td>
	  <td>{{ $video->title }}</td>
	  <td>{{ $video->id }}</td>
	  <td>{{ $video->duration }}</td>
	  <td>{{ $video->created_at }}</td>
	  <td><a onclick="editVideo({{ $video->id }})"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
	</tr>
	@endforeach
  </tbody>
</table>
{{ $videos->links() }}
@else
	<p class="text-center mt-5 mb-5" style="font-size:20px;">No video found</p>
@endif