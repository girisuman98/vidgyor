<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="{{ asset('css/compiled-4.5.6.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/loader.css') }}" rel="stylesheet">
	<style>
		#duration:valid { 
			border: 1px solid green;
		}
		
		#duration:invalid { 
			border: 1px solid red;
		}
		.pagination{
			float:right;
		}
	</style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!--{{ config('app.name', 'Laravel') }}-->
					<img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="Vidgyor" style="width: 60%;" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
	@if(Auth::check())
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/sweetalert2.js') }}"></script>
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script>
		$(document).ready(function () {
			$('#dtBasicExample').DataTable({
			   'paging'      : false,
			  'lengthChange': false,
			  'searching'   : false,
			  'ordering'    : true,
			  'info'        : false,
			  'autoWidth'   : true
			});
			$("#search").on('change keyup paste', function() {
				videosList();
			});
			$("#created_on").on('change', function() {
				videosList();
			});
			$("#duration").on('change keyup paste', function() {
				videosList();	
			});
			
			$("#add_video").submit(function(event){
				event.preventDefault(); //prevent default action 
				var post_url = $(this).attr("action"); //get form action url
				var request_method = $(this).attr("method"); //get form GET/POST method
				//var form_data = $(this).serialize(); //Encode form elements for submission
				
				$.ajax({
					url : post_url,
					type: request_method,
					data : new FormData($("#add_video")[0]),
					contentType: false,
					cache: false,
					processData: false,
					async: false,
					success : function(data){
						if(data == '1')
						{
							swal("Success!"," Saved Successfully", "success");
							$("#createVideo").modal('toggle');
							videosList();
						}
						else
						{
							swal("Error!", "Something went wrong", "error");
						}
					},
				});
			});
		});
		$( function() {
			$( "#created_on" ).datepicker({
			  dateFormat: "yy-mm-dd"
			});
		  });
	function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024 * 50; // in MB
        if (FileSize > 100) {
            swal("Error!", "File size exceeds 100 MB", "error");
           // $(file).val(''); //for clearing with Jquery
        } else {

        }
    }
	function ValidateSizeImage(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 2) {
            swal("Error!", "File size exceeds 2 MB", "error");
           // $(file).val(''); //for clearing with Jquery
        } else {

        }
    }
	function videosList(page = 0) {
        var caste = $("#videos_list");
        caste.empty();
        caste.append("<div class='text-center' style='padding-top: 5%;padding-bottom: 5%;'><div class='lds-roller'><div></div><div></div><div></div><div></div></div></div>");
      
        var search_word = '';
        if ($('#search').val()) {
            search_word = $('#search').val();
        }
        var created_on = '';
        if ($('#created_on').val()) {
            created_on = $('#created_on').val();
        }
        var duration = '';
        if ($('#duration').val()) {
            duration = $('#duration').val();
        }
		if(page == 0)
			url = "{{ Request::root() }}/video-lists-fetch";
		else 
			url = "{{ Request::root() }}/video-lists-fetch?page="+page;
        var params = {search_word: search_word,created_on: created_on,duration : duration,_token: '{{ csrf_token() }}'};
        $.ajax({
            type: 'POST',
            url: url,
            data : params,
            success : function(data){
                caste.empty();
                caste.html(data);
            },
        });
    }
	videosList();
	function editVideo(id) {
        var params = {id: id,_token: '{{ csrf_token() }}'};
        $.ajax({
            type: 'POST',
            url: "{{ route('video-edit-details') }}",
            data : params,
            success : function(data){
				$("#edit_modal_body").html(data);
                $("#editVideoModal").modal('toggle');
            },
        });
    }
	$(document).on('click', '.pagination a',function(event)
	{
		event.preventDefault();

		$('li').removeClass('active');
		$(this).parent('li').addClass('active');

		var myurl = $(this).attr('href');
		var page=$(this).attr('href').split('page=')[1];

		videosList(page)
	});
	</script>
	@endif
</body>
</html>
