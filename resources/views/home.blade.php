@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3 mt-1"><h6 class="font-weight-bold pt-2" style="color:#271284;">Content Manager <span style="padding: 2px 4px;background: red;color: rgb(255, 255, 255);font-size: 14px;">114</span></h6></div>
		<div class="col-md-3 mt-1">
			<div class="input-group form-sm form-2 pl-0">
			  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Search" id="search" aria-label="Search">
			  <div class="input-group-append">
				<span class="input-group-text lighten-3" id="basic-text1"><i class="fa fa-search text-grey"
					aria-hidden="true"></i></span>
			  </div>
			</div>
		</div>
		<div class="col-md-2 mt-1 p-0">
			<div class="input-group form-sm form-2 pl-0">
			  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Create On" id="created_on" aria-label="Create On">
			</div>
		</div>
		<div class="col-md-2 mt-1 pr-0">
			<div class="input-group form-sm form-2 pl-0">
			  <input class="form-control my-0 py-1 red-border" type="text" pattern="([0-5]{1}[0-9]{1}:){0,2}[0-5]{0,1}[0-9]{1}(\.\d+)?" placeholder="Duration in 00:00:00 format" id="duration" aria-label="Duration">
			</div>
		</div>
		<div class="col-md-2">
			<button type="button" data-target="#createVideo" data-toggle="modal" class="btn" style="background: rgb(39, 18, 132);font-size: 11px;padding: 10px 37px;">Create Video</button>
		</div>
	</div>
	<hr style="" />
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body" style="background: #EAF0F6;">
					<a href="#" class="mb-3" style="color:gray;">Videos</a><br /><br />
					<a href="#" class="mb-3" style="color:#000;">Categories</a><br /><br />
					<a href="#" style="color:#000;">Playlists</a>
                </div>
            </div>
        </div>
		<div class="col-md-9">
            <div class="card" id="videos_list">
                
            </div>
        </div>
    </div>
</div>

<div class="modal fade right" id="createVideo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 450px">
        <div class="modal-content">
            <div class="modal-header py-2">
                <h6 class="font-weight-bold pt-2" style="color:#271284;">Create Video</h6>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body scrollable" style="overflow-y: auto">
                <div class="wrapper-md">
					<span id="status_div"></span>
					<form action="{{ route('video-create') }}" method="post" enctype="multipart/form-data" id="add_video">
					@csrf
						<div class="input-group">
						  <div class="input-group-prepend">
							<span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
						  </div>
						  <div class="custom-file">
							<input type="file" class="custom-file-input" name="file_video" id="file_video"
							  aria-describedby="inputGroupFileAddon01" required accept="video/*" onchange="ValidateSize(this)">
							<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
						  </div>
						</div>
						<div class="input-group form-sm form-2 pl-0 mt-3">
						  <input class="form-control my-0 py-1 red-border" type="text" pattern="([0-5]{1}[0-9]{1}:){0,2}[0-5]{0,1}[0-9]{1}(\.\d+)?" placeholder="Duration in 00:00:00 format" name="duration_video" id="duration_video" aria-label="Duration" required>
						</div>
						<button type="submit" class="btn pull-right" style="background: rgb(39, 18, 132);font-size: 11px;padding: 10px 37px;">Create</button>
					</form>                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade right" id="editVideoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header py-2">
                <h6 class="font-weight-bold pt-2" style="color:#271284;">Update Metadata</h6>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body scrollable" style="overflow-y: auto" id="edit_modal_body">
                
            </div>
        </div>
    </div>
</div>
@endsection
